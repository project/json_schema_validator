# JSON Schema Validator

This module integrates [Opis JSON Schema](https://opis.io/json-schema) with Drupal.

You can validate JSON data against a JSON schema that you define.

This is very useful for validating the content of [JSON fields](https://www.drupal.org/project/json_field).

The module is intended for developers, so there is no UI. You have to use Drush instead.

## Getting started

Install the module with Composer.

Create some JSON schema. **All JSON schema must use the same domain for `"$id"`.**

Put all the JSON schema in the same directory.

Enable the module: `drush pm-enable json_schema_validator`

Go to the module settings page at `/admin/config/system/json_schema_validator`.

For **Schema Domain**, set the domain of the JSON schema (the domain used for `"$id"`).

For **Schema Directory Path**, set the path of the directory to your JSON schemas.


## Using the validator

Just call the service and use the `"$id"` of your schema without the domain and the `schema.json` suffix.

For a JSON schema with an `"$id"` of `https://www.example.com/my-defined-json-schema.schema.json`,
use `my-defined-json-schema`.

```
\Drupal::service('json_schema_validator.validator')->validateJsonSchema($json_array_to_validate, 'my-defined-json-schema');
```
