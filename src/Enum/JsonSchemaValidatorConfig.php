<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator\Enum;

/**
 * The settings on the Json Schema Validator module config form.
 */
enum JsonSchemaValidatorConfig: string {
  case SchemaDirectoryPath = 'schema_directory_path';
  case SchemaDomain = 'schema_domain';
  case ValidationOn = 'validator_on';
}
