<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator\Enum;

use Opis\JsonSchema\Errors\ValidationError;
use Opis\JsonSchema\Resolvers\SchemaResolver;

/**
 * Lists classes that are asserted in phpstan.
 */
enum AssertClass: string {
  case SchemaResolver = SchemaResolver::class;
  case ValidationError = ValidationError::class;
}
