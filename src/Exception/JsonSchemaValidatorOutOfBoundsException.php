<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator\Exception;

/**
 * Out of bounds exception for the Json Schema Validator module.
 */
class JsonSchemaValidatorOutOfBoundsException extends JsonSchemaValidatorException {
}
