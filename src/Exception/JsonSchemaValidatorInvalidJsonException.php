<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator\Exception;

/**
 * Invalid JSON exception for the Json Schema Validator Module.
 */
class JsonSchemaValidatorInvalidJsonException extends JsonSchemaValidatorException {
}
