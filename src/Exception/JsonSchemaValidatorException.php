<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator\Exception;

/**
 * Exceptions for the JSON Schema Validator module.
 */
class JsonSchemaValidatorException extends \Exception {
}
