<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\json_schema_validator\Enum\JsonSchemaValidatorConfig;

/**
 * Configures the JSON Schema Validator module..
 */
class JsonSchemaValidatorConfigurationForm extends ConfigFormBase {

  const FORM_ID = 'json_schema_validator.settings';

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function getFormId(): string {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function getEditableConfigNames(): array {
    return [self::FORM_ID];
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::FORM_ID);

    $form[self::FORM_ID] = [
      '#type' => 'details',
      '#title' => $this->t('Configure JSON Schema Validator'),
      '#open' => TRUE,
    ];

    $form[self::FORM_ID][JsonSchemaValidatorConfig::ValidationOn->value] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Validation Enabled'),
      '#description' => $this->t('Whether validation is enabled.'),
      '#default_value' => $config->get(JsonSchemaValidatorConfig::ValidationOn->value),
    ];

    $form[self::FORM_ID][JsonSchemaValidatorConfig::SchemaDomain->value] = [
      '#type' => 'textfield',
      '#title' => $this->t('Schema Domain'),
      '#description' => $this->t('Domain of developer-defined schemas. Include http:// or https://. No trailing slash.'),
      '#size' => 128,
      '#default_value' => $config->get(JsonSchemaValidatorConfig::SchemaDomain->value),
      '#required' => TRUE,
      '#maxlength' => 255,
    ];

    $form[self::FORM_ID][JsonSchemaValidatorConfig::SchemaDirectoryPath->value] = [
      '#type' => 'textfield',
      '#title' => $this->t('Schema Directory Path'),
      '#description' => $this->t('The path to the directory containing the developer-defined schemas. All schemas must be in this directory.'),
      '#size' => 128,
      '#default_value' => $config->get(JsonSchemaValidatorConfig::SchemaDirectoryPath->value),
      '#required' => TRUE,
      '#maxlength' => 256,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config(self::FORM_ID);
    $config
      ->set(JsonSchemaValidatorConfig::ValidationOn->value, $form_state->getValue(JsonSchemaValidatorConfig::ValidationOn->value))
      ->set(JsonSchemaValidatorConfig::SchemaDomain->value, $form_state->getValue(JsonSchemaValidatorConfig::SchemaDomain->value))
      ->set(JsonSchemaValidatorConfig::SchemaDirectoryPath->value, $form_state->getValue(JsonSchemaValidatorConfig::SchemaDirectoryPath->value))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $settings = $form_state->getValues();
    $schema_directory_path = $settings[JsonSchemaValidatorConfig::SchemaDirectoryPath->value];

    if (!is_dir($schema_directory_path)) {
      $form_state->setErrorByName('invalid_path', $this->t('There is no directory at the specified location.'));
      return;
    }

    $schema_domain = $settings[JsonSchemaValidatorConfig::SchemaDomain->value];
    if (!str_starts_with((string) $schema_domain, 'https://') && !str_starts_with((string) $schema_domain, 'http://')) {
      $form_state->setErrorByName('domain_needs_http', $this->t('The schema domain must start with http:// or https://.'));
      return;
    }
    if (str_ends_with((string) $schema_domain, '/')) {
      $form_state->setErrorByName('domain_no_trailing_slash', $this->t('The schema domain cannot end with a slash.'));
      return;
    }

  }

}
