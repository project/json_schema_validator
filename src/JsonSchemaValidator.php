<?php

declare(strict_types=1);

namespace Drupal\json_schema_validator;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\json_schema_validator\Enum\AssertClass;
use Drupal\json_schema_validator\Enum\JsonSchemaValidatorConfig;
use Drupal\json_schema_validator\Exception\JsonSchemaValidatorInvalidJsonException;
use Drupal\json_schema_validator\Exception\JsonSchemaValidatorOutOfBoundsException;
use Opis\JsonSchema\Errors\ErrorFormatter;
use Opis\JsonSchema\ValidationResult;
use Opis\JsonSchema\Validator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Webmozart\Assert\Assert;

/**
 * Handles JSON validation exceptions.
 */
final class JsonSchemaValidator {

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * Creates a JsonSchemaValidator instance.
   */
  public static function create(ContainerInterface $container): JsonSchemaValidator {
    return new self(
      $container->get('config.factory'),
    );
  }

  /**
   * Throws an exception when JSON is invalid according to JSON schema.
   */
  public static function throwInvalidJsonException(ValidationResult $result, string $what_failed_to_validate, array $json_array): never {
    $error = $result->error();
    Assert::isInstanceOf($error, AssertClass::ValidationError->value);
    $formatter = new ErrorFormatter();

    // Print helper.
    $print = fn($value) => json_encode(
      $value,
      JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
    );
    $default_multiple_message = $print($formatter->format($error, TRUE));
    $error_message = $print($formatter->formatOutput($error, 'verbose')) . $default_multiple_message;
    $json_debug_data = json_encode($json_array);
    $uid = \Drupal::currentUser()->id();
    throw new JsonSchemaValidatorInvalidJsonException("$what_failed_to_validate failed to validate!\n
      ||\n
      uid: $uid
      ||\n
      $error_message\n
      ||\n
      JSON data: $json_debug_data");
  }

  /**
   * Perform validation of the given JSON schema.
   *
   * @param string $encoded_json
   *   The stringified JSON.
   * @param string $schema
   *   The relevant JSON schema.
   *
   * @return bool
   *   TRUE if validation succeeded. Throws an exception otherwise.
   */
  public function validateJsonSchema(string $encoded_json, string $schema): bool {
    $json_schema_validator_config = $this->configFactory->get('json_schema_validator.settings');
    $validation_on = (bool) $json_schema_validator_config->get(JsonSchemaValidatorConfig::ValidationOn->value);
    if ($validation_on === FALSE) {
      // Validation is disabled, so validation should automatically pass.
      return TRUE;
    }
    $validator = new Validator();
    $resolver = $validator->loader()->resolver();
    Assert::isInstanceOf($resolver, AssertClass::SchemaResolver->value);
    $schema_base_url = $json_schema_validator_config->get(JsonSchemaValidatorConfig::SchemaDomain->value);
    $schema_directory_path = $json_schema_validator_config->get(JsonSchemaValidatorConfig::SchemaDirectoryPath->value);
    Assert::stringNotEmpty($schema_directory_path, JsonSchemaValidatorConfig::SchemaDirectoryPath->value . ' not set!');
    $resolver->registerPrefix($schema_base_url . '/', $schema_directory_path);
    $decoded_json = json_decode($encoded_json, FALSE);
    $result = $validator->validate($decoded_json, $schema_base_url . "/$schema.schema.json");
    if ($result->isValid()) {
      $encoded_json = json_encode($decoded_json);
      Assert::stringNotEmpty($encoded_json, "Failed to encode JSON schema $schema!");
      return TRUE;
    }
    $decoded_json_associative_array = json_decode($encoded_json, TRUE);
    if (!is_array($decoded_json_associative_array)) {
      throw new JsonSchemaValidatorOutOfBoundsException("Failed to decode encoded JSON! $encoded_json");
    }
    JsonSchemaValidator::throwInvalidJsonException($result, $schema, $decoded_json_associative_array);
  }

}
